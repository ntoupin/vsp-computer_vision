import numpy as np
import cv2

cap = cv2.VideoCapture('Video_Cropped.mp4')

while(cap.isOpened()):

    # Capture frame-by-frame
    Status, Frame_Original = cap.read()

    # Exit if video is finished
    if Status == False:
        break

    # Convert BGR to HSV
    Frame_Hsv = cv2.cvtColor(Frame_Original, cv2.COLOR_BGR2HSV)

    # Filter color
    Lower_Color = np.array([0, 75, 20])
    Upper_Color = np.array([40, 255, 255])
    Frame_Mask = cv2.inRange(Frame_Hsv, Lower_Color, Upper_Color)

    #lower_blue = np.array([110,50,50])
    #upper_blue = np.array([130,255,255])

    # Bitwise-AND mask and original image
    #res = cv2.bitwise_and(frame,frame, mask= mask)

    # Our operations on the frame come here
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # detect circles in the image
    Circle_Array = cv2.HoughCircles(Frame_Mask, cv2.HOUGH_GRADIENT, 1.2, 500)
 
    # ensure at least some circles were found
    if Circle_Array is not None:
	    # convert the (x, y) coordinates and radius of the circles to integers
        Circle_Array = np.round(Circle_Array[0, :]).astype("int")

	    # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in Circle_Array:
		    # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(Frame_Mask, (x, y), r, (0, 255, 0), 4)
            #cv2.rectangle(frame, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
            print("Circle detected at " + x + "," + y)

    # Display the resulting frame
    #cv2.imshow('Original Frame', Frame_Original)
    #cv2.imshow('Hsv Frame', Frame_Hsv)
    cv2.imshow('Mask Frame', Frame_Mask)
    
    # Wait for q key
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()