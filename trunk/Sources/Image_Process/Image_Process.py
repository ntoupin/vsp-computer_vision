import cv2
import numpy as np

img = cv2.imread('test.png',0)
img = cv2.medianBlur(img,5)
cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

Circle_Array = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,param1=50,param2=30,minRadius=0,maxRadius=0)

if Circle_Array is not None:
    Circle_Array = np.uint16(np.around(Circle_Array))
    for i in Circle_Array[0,:]:
        cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
        cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
else:
    print('No circles found')

cv2.imshow('detected circles',cimg)
cv2.waitKey(0)
cv2.destroyAllWindows()