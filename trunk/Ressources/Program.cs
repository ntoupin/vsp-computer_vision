﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenCvSharp;

namespace FaceRecognition
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Execution started !");

            Thread captureImageThread = new Thread(CaptureImage);
            captureImageThread.Start();

            Console.ReadLine();
        }

        public static void CaptureImage()
        {
            Console.WriteLine("Image capture thread has started.");

            using (VideoCapture cap = VideoCapture.FromCamera(0))
            {
                Console.WriteLine("Image is " + cap.FrameWidth.ToString() + " by " + cap.FrameHeight.ToString() + ".");

                using (Window window = new Window("Capture Camera"))
                {
                    

                    while (true)
                    {
                        Mat image = new Mat();
                        cap.Read(image);

                        if (image.Empty())
                            break;

                        //Mat finalImage = FindRedCirclesInImage(image, 1, cap.FrameHeight / 2, 100, 20, cap.FrameHeight * 0.1, cap.FrameHeight * 0.8);

                        //window.ShowImage(finalImage);
                        window.ShowImage(image);

                        Cv2.WaitKey(15);
                        image.Dispose();
                    }
                    cap.Release();
                }
            }
        }

        public static Mat FindRedCirclesInImage(Mat originalMat, double resRatio, double minDistance, double upperTreshold,
            double centerDetector, double minRadius, double maxRadius)
        {
            try
            {
                //Cv2.ImShow("Original image", originalMat);

                Mat processMat = new Mat();
                Cv2.MedianBlur(originalMat, processMat, 3);
                //Cv2.ImShow("MedianBlur filter image", processMat);

                // Range de couleurs
                Mat hsvImage = new Mat();
                Cv2.CvtColor(processMat, hsvImage, ColorConversionCodes.BGR2HSV);

                Mat lowRedHueRange = new Mat();
                Scalar lowerRangeLow = new Scalar(0, 100, 100);
                Scalar upperRangeLow = new Scalar(10, 255, 255);
                Cv2.InRange(hsvImage, lowerRangeLow, upperRangeLow, lowRedHueRange);

                Mat highRedHueRange = new Mat();
                Scalar lowerRangeHigh = new Scalar(150, 100, 100);
                Scalar upperRangeHigh = new Scalar(179, 255, 255);
                Cv2.InRange(hsvImage, lowerRangeHigh, upperRangeHigh, highRedHueRange);

                Mat redHueImage = new Mat();
                Cv2.AddWeighted(lowRedHueRange, 1, highRedHueRange, 1, 0, redHueImage);
                //Cv2.ImShow("Red filter image", redHueImage);

                //Filtre Gaussien
                Size gaussianSize = new Size(9, 9);
                Cv2.GaussianBlur(redHueImage, redHueImage, gaussianSize, 2, 2);
                //Cv2.ImShow("Gaussian filter image", redHueImage);

                // Detection de cercle
                CircleSegment[] cercles = Cv2.HoughCircles(redHueImage, HoughMethods.Gradient, resRatio,
                    minDistance, upperTreshold, centerDetector, (int)minRadius, (int)maxRadius);

                // Ajout des cercles dans l'image initiale
                foreach (CircleSegment element in cercles)
                {
                    originalMat.Circle(element.Center, (int)element.Radius, Scalar.GreenYellow, 4);
                    Console.WriteLine("Circle found! Center : " + element.Center.X + ", " + element.Center.Y + " - Radius : " + element.Radius);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error processing image at : " + DateTime.Now.ToLongTimeString() + " Erreur : " + e.Message);
            }

            return originalMat;
        }
    }
}
